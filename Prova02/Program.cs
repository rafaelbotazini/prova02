﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova02
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                GerenciadorPatrimonio.CadastrarBem(new Computador(5000, "Computador 1 - Intel i7 - 500GB HD", 16));
                GerenciadorPatrimonio.CadastrarBem(new Computador(3200, "Computador 2 - Intel i5 - 500GB HD", 16));
                GerenciadorPatrimonio.CadastrarBem(new Computador(2000, "Computador 3 - Intel i3 - 500GB HD", 8));

                GerenciadorPatrimonio.CadastrarBem(new Mobilia(500, "Cadeira Grande, branca #1", "Cadeira"));
                GerenciadorPatrimonio.CadastrarBem(new Mobilia(500, "Cadeira Grande, branca #2", "Cadeira"));
                GerenciadorPatrimonio.CadastrarBem(new Mobilia(500, "Cadeira Grande, branca #3", "Cadeira"));
                GerenciadorPatrimonio.CadastrarBem(new Mobilia(500, "Cadeira Grande, branca #4", "Cadeira"));
                GerenciadorPatrimonio.CadastrarBem(new Mobilia(2600, "Mesa jantar branca", "Mesa"));

                GerenciadorPatrimonio.CadastrarBem(new Computador(0, "Este vai gerar um erro e não será cadastrado", 321));
            }
            catch (Exception e)
            {
                Console.WriteLine("\n\nUm erro ocorreu: {0}\n\n", e.Message);
            }
            finally
            {
                Console.WriteLine(GerenciadorPatrimonio.RelatorioPatrimonio());
            }

            Console.ReadKey();

        }
    }
}
