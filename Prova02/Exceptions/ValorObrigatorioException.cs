﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova02
{
    class ValorObrigatorioException : ArgumentException
    {
        public ValorObrigatorioException() 
            : base("O valor do patrimônio deve ser maior que 0")
        {
        }
    }
}
