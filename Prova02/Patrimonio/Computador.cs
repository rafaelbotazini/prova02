﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova02
{
    class Computador : Patrimonio
    {
        protected override string Tipo { get { return "Computador"; } }

        public int QuantidadeMemoria { get; private set; }
        
        public Computador(double valor, string descricaoDetalhada, int quantidadeMemoria) 
            : base(valor, descricaoDetalhada)
        {
            QuantidadeMemoria = quantidadeMemoria;
        }

        public override string Ficha()
        {
            return base.Ficha() +
                "Quantidade de Memória RAM: " + QuantidadeMemoria + "GB\n";
        }
    }
}
