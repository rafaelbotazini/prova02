﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova02
{
    static class GerenciadorPatrimonio
    {
        private static List<Patrimonio> listaBens = new List<Patrimonio>();

        public static int QuantidadeBens { get { return listaBens.Count; } }

        public static double ValorTotalPatrimonio { get; private set; }

        public static void CadastrarBem(Patrimonio patrimonio)
        {
            ValorTotalPatrimonio += patrimonio.Valor;
            
            listaBens.Add(patrimonio);
        }

        public static string RelatorioPatrimonio()
        {
            string ficha = "********* Relatório de todos os bens cadastrados ********* \n\n";

            ficha += "Total de bens: " + QuantidadeBens + "\n";
            ficha += string.Format("Valor total do patrimônio: {0:C}\n\n", ValorTotalPatrimonio);

            ficha += "Relação detalhada dos bens: \n";

            foreach (var item in listaBens)
            {
                ficha += "\n============\n\n";

                ficha += item.Ficha() + "\n";
            }

            if (listaBens.Count == 0)
                ficha += "\nNenhum item adicionado até o momento.";

            return ficha;
        }
    }
}
