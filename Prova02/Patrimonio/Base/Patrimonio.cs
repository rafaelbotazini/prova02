﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova02
{
    abstract class Patrimonio
    {
        protected abstract string Tipo { get; }

        public int Numero { get; private set; }
        public string DescricaoDetalhada { get; private set; }
        public double Valor { get; private set; }

        protected Patrimonio(double valor, string descricaoDetalhada)
        {
            if (valor <= 0)
                throw new ValorObrigatorioException();

            Numero = GerenciadorPatrimonio.QuantidadeBens + 1;
            DescricaoDetalhada = descricaoDetalhada;
            Valor = valor;
        }

        public virtual string Ficha()
        {
            return
                Tipo + " #" + Numero + "\n\n" +
                string.Format("Valor do item: {0:C}", Valor) + "\n" +
                "Descrição detalhada: " + DescricaoDetalhada + "\n";
        }
    }
}
