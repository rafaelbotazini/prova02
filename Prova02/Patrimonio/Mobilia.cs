﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova02
{
    class Mobilia : Patrimonio
    {
        protected override string Tipo { get { return "Mobília"; } }

        public string Descricao { get; private set; }

        public Mobilia(double valor, string descricaoDetalhada, string descricao) 
            : base(valor, descricaoDetalhada)
        {
            Descricao = descricao;
        }

        public override string Ficha()
        {
            return base.Ficha() + 
                "Descrição da mobília: " + Descricao + "\n";
        }
    }
}
